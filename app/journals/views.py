from flask import Flask, render_template, request
import json
from . import journals
from app.dao import data_access_object_journal
import pandas as pd
import datetime

@journals.route("/journals", methods=["GET", "POST"])

def input():
    journal_title = ""
    xml_text = ""
    req = "GET"
    if request.method == "POST":
        print("post")
        req = "POST"
        if request.form["action"] == "Submit":
            journal_title = request.form["title"]
            issn = request.form["issn"]
            journal_doi = request.form["jdoi"]
            journal_url = request.form["juri"]
            volume = request.form["volume"]
            issue = request.form["issue"]
            date = datetime.datetime.strptime(request.form["date"],'%Y-%m-%d')
            dict_form = {
                "full_title": journal_title, 
                "issn": issn, 
                "doi": journal_doi, 
                "resource": journal_url, 
                "volume": volume, 
                "issue": issue,
                "date": date
                }
            file = request.files.get('file')
            xml_text = data_access_object_journal.DataAccess().create_xml(dict_form, file)

                    
    return render_template("journals.html", journal_title = journal_title, xml_text = xml_text)