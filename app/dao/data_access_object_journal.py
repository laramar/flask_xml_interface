from lxml import etree
import datetime
import pandas as pd

#https://stackoverflow.com/questions/51356402/how-to-upload-excel-or-csv-file-to-flask-as-a-pandas-data-frame

class DataAccess:
    def create_xml(self, dict_form, file):
        current_timestamp = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
        #current_date = datetime.datetime.now().strftime('%Y-%m-%d')
        date_published = dict_form["date"]
        year = date_published.strftime("%Y")
        month = date_published.strftime("%m")
        day = date_published.strftime("%d")

        # create XML 

        #set root namespaces
        MY_NAMESPACES={'xsi': 'http://www.w3.org/2001/XMLSchema-instance', None: 'http://www.crossref.org/schema/4.4.2', 'jats': 'http://www.ncbi.nlm.nih.gov/JATS1', 'fr': 'http://www.crossref.org/fundref.xsd'}
        attr_xsi = etree.QName("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation")
        root = etree.Element('doi_batch', {attr_xsi: "http://www.crossref.org/doi_resources_schema/4.4.2 http://www.crossref.org/schema/deposit/doi_resources4.4.2.xsd"}, nsmap=MY_NAMESPACES, version="4.4.2")

        #create head
        head = etree.SubElement(root, 'head')

        #set head values
        doi_batch_id = etree.SubElement(head, 'doi_batch_id').text = dict_form["full_title"].replace(" ", "")+current_timestamp #TO BE MODIFIED
        timestamp = etree.SubElement(head, "timestamp").text = current_timestamp

        depositor = etree.SubElement(head, 'depositor')
        etree.SubElement(depositor, 'depositor_name').text = "Bononia:bononia"
        etree.SubElement(depositor, 'email_address').text = "journals@buponline.com"

        etree.SubElement(head, 'registrant').text = "Bononia"

        #create body

        body = etree.SubElement(root, 'body')

        #set body values
        journal = etree.SubElement(body, 'journal')
        j_data = etree.SubElement(journal, 'journal_metadata', language="it")
        etree.SubElement(j_data, "full_title").text = dict_form["full_title"]
        etree.SubElement(j_data, 'issn', media_type='electronic').text = dict_form["issn"]

        doi_data = etree.SubElement(j_data, "doi_data")
        etree.SubElement(doi_data, "doi").text = dict_form["doi"]
        etree.SubElement(doi_data, "resource").text = dict_form["resource"]

        j_issue = etree.SubElement(journal, 'journal_issue')
        publication_date = etree.SubElement(j_issue, 'publication_date', media_type='online')
        month = etree.SubElement(publication_date, 'month').text = str(month)
        day = etree.SubElement(publication_date, 'day').text = str(day)
        year = etree.SubElement(publication_date, 'year').text = str(year)
        j_volume = etree.SubElement(j_issue, 'journal_volume')
        if j_volume !='nan':
            etree.SubElement(j_volume, 'volume').text = dict_form["volume"]
        if j_issue !='nan':
            etree.SubElement(j_issue, 'issue').text = dict_form["issue"]
        

        df = pd.read_csv(file, sep='\t') 
        df.fillna("nan",inplace=True)


        for row in df.itertuples():
            article = etree.SubElement(journal, "journal_article", publication_type="full_text")
            titles = etree.SubElement(article, "titles")
            etree.SubElement(titles, "title").text = row.Title #LOOP
            #in titles it's possible to put also the <original_language_title language="it">
            contributors = etree.SubElement(article, "contributors")

            if row.Authors !='nan':
                list_authors = row.Authors.split(',') #already a list in pandas
                name = etree.SubElement(contributors, "person_name", sequence="first", contributor_role="author")
                first_author = list_authors[0].split(" ")
                etree.SubElement(name, "given_name").text = first_author[0]
                etree.SubElement(name, "surname").text = " ".join(first_author[1:])
                if list_authors[1:] is not None:
                    for el in list_authors[1:]:
                        other_author = [x for x in el.split(" ") if x.strip()] #remove empty strings from list (spaces) https://stackoverflow.com/questions/3845423/remove-empty-strings-from-a-list-of-strings
                        name_seq = etree.SubElement(contributors, "person_name", sequence="additional", contributor_role="author")
                        etree.SubElement(name_seq, "given_name").text = other_author[0]
                        etree.SubElement(name_seq, "surname").text = " ".join(other_author[1:])

            jats_namespace = "http://www.ncbi.nlm.nih.gov/JATS1"
            etree.register_namespace("jats", jats_namespace)
            if str(row.Abstract) != "nan": #if there is no abstract
                abstract = etree.SubElement(article, etree.QName(jats_namespace, "abstract"))
                etree.SubElement(abstract, etree.QName(jats_namespace, "p")).text = row.Abstract

            publication = etree.SubElement(article, "publication_date", media_type="online")
            etree.SubElement(publication, "month"). text = month
            etree.SubElement(publication, "day").text = day
            etree.SubElement(publication, "year").text = year

            license_namespace = {None: "http://www.crossref.org/AccessIndicators.xsd"}
            license = etree.SubElement(article, "program", nsmap=license_namespace)
            etree.SubElement(license, "free_to_read")
            etree.SubElement(license, "license_ref", applies_to="vor", start_date=year+"-"+month+"-"+day).text = "https://creativecommons.org/licenses/by/4.0/"


            article_doi_data = etree.SubElement(article, "doi_data")
            etree.SubElement(article_doi_data, "doi").text = row.DOI 
            etree.SubElement(article_doi_data, "resource", content_version="vor").text = row.Link

        s = etree.tostring(root, pretty_print=True, encoding='utf-8',xml_declaration = True).decode('utf8')
        return s

        #manca: alternative title