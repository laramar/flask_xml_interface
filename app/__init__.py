from flask import Flask, render_template, request

app = Flask(__name__, template_folder='templates', static_folder='static')
app.debug = True

from .home import home as home_blueprint
app.register_blueprint(home_blueprint)

from .journals import journals as journals_blueprint
app.register_blueprint(journals_blueprint)